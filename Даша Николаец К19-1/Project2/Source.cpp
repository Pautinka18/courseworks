#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <Windows.h>
using namespace std;

class Number {
public: int number = 0;
};

struct service_officer
{
    string fio = "";
    string rank = "";
    int year = -1;
    string education = "";
    int experience = -1;
    double money = -1;
};

Number number;

unsigned char p = 'a';
int num = -1;
double y = -1;
string str = "";

service_officer base[100];

int create(int k) {
    system("cls");
    ofstream f;
    f.open("binary.txt", ios::binary);
    int kol;
    cout << "������� ������� �� ������ ������ : ";
    cin >> kol;
    service_officer* temp = new service_officer[kol];
    for (int i = 0; i < kol; i++) {
        cin.get();
        while (!isupper(p)) {
            cout << "������� �.�.�. (������ ����� ���������, ������ : �������� �.�.) : ";
            getline(cin, temp[i].fio);
            p = temp[i].fio[0];
        }
        p = 'a';
        while (!isupper(p)) {
            cout << "������� ��������� : "; 
            getline(cin,temp[i].rank);
            p = temp[i].rank[0];
        }
        p = 'a';
        while (temp[i].year < 2000 || temp[i].year>2020) {        //���� �� ����� ������� ���������� �����, ��������� ����
            cout << "������� ��� ���������� �� ������ (2000 - 2020) : ";
            cin >> temp[i].year;
        }
        while (!isupper(p)) {
            cout << "������� ����������� : "; cin >> temp[i].education;
            p = temp[i].education[0];
        }p = 'a';
        while (temp[i].experience < 0 || temp[i].experience > 30) {        //���� �� ����� ������� ���������� �����, ��������� ����
            cout << "������� ���� (0 - 30) : ";//�������� �� ������ �����
            cin >> temp[i].experience;
        }
        while (temp[i].money < 0 || temp[i].money > 1000) {
            cout << "������� ����� (0 - 1000) : "; cin >> temp[i].money;
        }
        f.write((char*)&temp[i], sizeof(service_officer));
    }
    f.close();
    return k = 1;
}

int read(int k) {
    system("cls");
    ifstream in;
    in.open("base.txt");
    int i = 0;
    cout << "�" << setw(20) << "�.�.�." << setw(20) << "���������" << setw(30) << "��� ������������ � ������" << setw(15) << "�����������" << setw(12) << "����" << setw(13) << "�����" << endl;
    while (!in.eof()) {
        getline(in, base[i].fio);
        in >> base[i].rank;
        in >> base[i].year;
        in >> base[i].education;
        in >> base[i].experience;
        in >> base[i].money;
        cout << i + 1 << setw(20) << base[i].fio << setw(20) << base[i].rank << setw(20) << base[i].year << setw(23) << base[i].education << setw(12) << base[i].experience << setw(14) << base[i].money << endl;
        i++;
        in.ignore(1, '\n');
    }
    number.number = i;
    cout << endl;
    system("pause");
    return k = 1;
}

int search(int k) {
    system("cls");
    k = 0;
    cout << "�� ������ ���� �� ������ ����� �������� :" << endl;
    cout << "1. �.�.�." << endl;
    cout << "2. ���������." << endl;
    cout << "3. ��� ����������" << endl;
    cout << "4. �����������." << endl;
    cout << "5. ����." << endl;
    cout << "6. �����." << endl;
    cout << endl;
    while (k < 1 || k>6) {
        cout << "��� ����� (1-6) : ";
        cin >> k;
    }
    switch (k)
    {
    case 1: {
        system("cls");
        str = "";
        cin.get();
        while (!isupper(p)) {
            cout << "������� �.�.�. (������ ����� ���������, ������ : �������� �.�.) : ";
            getline(cin, str);
            p = str[0];
        }
        p = 'a';
        system("cls");
        cout << "�" << setw(20) << "�.�.�." << setw(20) << "���������" << setw(30) << "��� ������������ � ������" << setw(15) << "�����������" << setw(12) << "����" << setw(13) << "�����" << endl;
        for (int i = 0; i < number.number; i++) {
            if (str == base[i].fio) {
                cout << i + 1 << setw(20) << base[i].fio << setw(20) << base[i].rank << setw(20) << base[i].year << setw(23) << base[i].education << setw(12) << base[i].experience << setw(14) << base[i].money << endl;
            }
        }
        str = "";
        system("pause");
        break;
    }
    case 2: {
        system("cls");
        p = 'a';
        while (!isupper(p)) {
            cout << "������� ��������� : "; cin >> str;
            p = str[0];
        }
        p = 'a';
        system("cls");
        cout << "�" << setw(20) << "�.�.�." << setw(20) << "���������" << setw(30) << "��� ������������ � ������" << setw(15) << "�����������" << setw(12) << "����" << setw(13) << "�����" << endl;
        for (int i = 0; i < number.number; i++) {
            if (str == base[i].rank) {
                cout << i + 1 << setw(20) << base[i].fio << setw(20) << base[i].rank << setw(20) << base[i].year << setw(23) << base[i].education << setw(12) << base[i].experience << setw(14) << base[i].money << endl;
            }
        }
        str = "";
        system("pause");
        break;
    }
    case 3: {
        system("cls");
        while (num < 2000 || num>2020) {        //���� �� ����� ������� ���������� �����, ��������� ����
            cout << "������� ��� ���������� �� ������ (2000 - 2020) : ";
            cin >> num;
        }
        system("cls");
        cout << "�" << setw(20) << "�.�.�." << setw(20) << "���������" << setw(30) << "��� ������������ � ������" << setw(15) << "�����������" << setw(12) << "����" << setw(13) << "�����" << endl;
        for (int i = 0; i < number.number; i++) {
            if (num == base[i].year) {
                cout << i + 1 << setw(20) << base[i].fio << setw(20) << base[i].rank << setw(20) << base[i].year << setw(23) << base[i].education << setw(12) << base[i].experience << setw(14) << base[i].money << endl;
            }
        }
        num = -1;
        system("pause");
        break;
    }
    case 4: {
        system("cls");
        while (!isupper(p)) {
            cout << "������� ����������� : "; cin >> str;
            p = str[0];
        }
        system("cls");
        cout << "�" << setw(20) << "�.�.�." << setw(20) << "���������" << setw(30) << "��� ������������ � ������" << setw(15) << "�����������" << setw(12) << "����" << setw(13) << "�����" << endl;
        for (int i = 0; i < number.number; i++) {
            if (str == base[i].education) {
                cout << i + 1 << setw(20) << base[i].fio << setw(20) << base[i].rank << setw(20) << base[i].year << setw(23) << base[i].education << setw(12) << base[i].experience << setw(14) << base[i].money << endl;
            }
        }
        str = "";
        system("pause");
        break;
    }
    case 5: {
        system("cls");
        while (num < 0 || num>30) {        //���� �� ����� ������� ���������� �����, ��������� ����
            cout << "������� ���� (0 - 30) : ";//�������� �� ������ �����
            cin >> num;
        }
        system("cls");
        cout << "�" << setw(20) << "�.�.�." << setw(20) << "���������" << setw(30) << "��� ������������ � ������" << setw(15) << "�����������" << setw(12) << "����" << setw(13) << "�����" << endl;
        for (int i = 0; i < number.number; i++) {
            if (num == base[i].experience) {
                cout << i + 1 << setw(20) << base[i].fio << setw(20) << base[i].rank << setw(20) << base[i].year << setw(23) << base[i].education << setw(12) << base[i].experience << setw(14) << base[i].money << endl;
            }
        }
        system("pause");
        break;
    }
    case 6: {
        system("cls");
        while (y < 0 || y>1000) {
            cout << "������� ����� (0 - 1000) : "; cin >> y;
        }
        system("cls");
        cout << "�" << setw(20) << "�.�.�." << setw(20) << "���������" << setw(30) << "��� ������������ � ������" << setw(15) << "�����������" << setw(12) << "����" << setw(13) << "�����" << endl;

        for (int i = 0; i < number.number; i++) {
            if (y == base[i].money) {
                cout << i + 1 << setw(20) << base[i].fio << setw(20) << base[i].rank << setw(20) << base[i].year << setw(23) << base[i].education << setw(12) << base[i].experience << setw(14) << base[i].money << endl;
            }
        }
        system("pause");
        break;
    }
    }
    return k = 1;
}

int add(int k) {
    system("cls");
    ofstream fin;
    fin.open("base.txt", ios::app);
    cout << "������� ����� �������� �� ������ �������� : "; cin >> k;
    for (int i = 0; i < k; i++) {
        system("cls");
        cout << i + 1 << " ������������." << endl;
        cin.get();
        while (!isupper(p)) {
            cout << "������� �.�.�. (������ ����� ���������, ������ : �������� �.�.) : ";
            getline(cin, str);
            p = str[0];
        }
        fin << str;
        fin << "\n";
        str = "";
        p = 'a';
        while (!isupper(p)) {
            cout << "������� ��������� (����������, �������, ������, ���������, ���������): "; cin >> str;
            p = str[0];
        } fin << str;
        p = 'a';
        str = "";
        fin << "\n";
        while (num < 2000 || num>2020) {        //���� �� ����� ������� ���������� �����, ��������� ����
            cout << "������� ��� ���������� �� ������ (2000 - 2020) : ";
            cin >> num;
        } fin << num;
        num = -1;
        fin << "\n";
        while (!isupper(p)) {
            cout << "������� ����������� : "; cin >> str;
            p = str[0];
        } fin << str;
        p = 'a';
        str = "";
        fin << "\n";
        while (num < 0 || num>30) {        //���� �� ����� ������� ���������� �����, ��������� ����
            cout << "������� ���� (0 - 30) : ";//�������� �� ������ �����
            cin >> num;
        } fin << num;
        num = -1;
        fin << "\n";
        while (y < 0 || y>1000) {
            cout << "������� ����� (0 - 1000) : "; cin >> y;
        }
        fin << y;
        y = -1;
    }
    return k = 1;
}

int sorting(int k) {
    system("cls");
    k = 0;
    cout << "�� ������ ���� ������ �������������?" << endl;
    cout << "1. ���." << endl;
    cout << "2. ��� ����������." << endl;
    cout << "3. ���������." << endl;
    cout << "4. ����." << endl;
    cout << "5. �����." << endl;
    cout << "6. �����������." << endl;
    cout << endl;
    while (k < 1 || k>6) { cout << "��� ����� (1-6) : "; cin >> k; }
    switch (k) {
    case 1: {
        system("cls");
        cout << "�" << setw(20) << "�.�.�." << setw(20) << "���������" << setw(30) << "��� ������������ � ������" << setw(15) << "�����������" << setw(12) << "����" << setw(13) << "�����" << endl;
        for (int i = 0; i < number.number; i++) {
            for (int j = i + 1; j < number.number; j++)
                if (strcmp(base[i].fio.c_str(), base[j].fio.c_str()) > 0) {
                    swap(base[i].fio, base[j].fio);
                    swap(base[i].rank, base[j].rank);
                    swap(base[i].year, base[j].year);
                    swap(base[i].experience, base[j].experience);
                    swap(base[i].education, base[j].education);
                    swap(base[i].money, base[j].money);
                    j--;
                } 
            cout << i + 1 << setw(20) << base[i].fio << setw(20) << base[i].rank << setw(20) << base[i].year << setw(23) << base[i].education << setw(12) << base[i].experience << setw(14) << base[i].money << endl;
        }
        system("pause");
        break;
    }
    case 2: {
        system("cls");
        cout << "�" << setw(20) << "�.�.�." << setw(20) << "���������" << setw(30) << "��� ������������ � ������" << setw(15) << "�����������" << setw(12) << "����" << setw(13) << "�����" << endl;
        for (int i = 0; i < number.number; i++) {
            for (int j = i + 1; j < number.number; j++)
                if (base[i].year > base[j].year) {
                    swap(base[i].fio, base[j].fio);
                    swap(base[i].rank, base[j].rank);
                    swap(base[i].year, base[j].year);
                    swap(base[i].experience, base[j].experience);
                    swap(base[i].education, base[j].education);
                    swap(base[i].money, base[j].money);
                    j--;
                }
            cout << i + 1 << setw(20) << base[i].fio << setw(20) << base[i].rank << setw(20) << base[i].year << setw(23) << base[i].education << setw(12) << base[i].experience << setw(14) << base[i].money << endl;
        }
        system("pause");
        break;
    }
    case 3: {
        system("cls");
        cout << "�" << setw(20) << "�.�.�." << setw(20) << "���������" << setw(30) << "��� ������������ � ������" << setw(15) << "�����������" << setw(12) << "����" << setw(13) << "�����" << endl;
        for (int i = 0; i < number.number; i++) {
            for (int j = i + 1; j < number.number; j++)
                if (strcmp(base[i].rank.c_str(), base[j].rank.c_str()) > 0) {
                    swap(base[i].fio, base[j].fio);
                    swap(base[i].rank, base[j].rank);
                    swap(base[i].year, base[j].year);
                    swap(base[i].experience, base[j].experience);
                    swap(base[i].education, base[j].education);
                    swap(base[i].money, base[j].money);
                    j--;
                }
            cout << i + 1 << setw(20) << base[i].fio << setw(20) << base[i].rank << setw(20) << base[i].year << setw(23) << base[i].education << setw(12) << base[i].experience << setw(14) << base[i].money << endl;
        }
        system("pause");
        break;
    }
    case 4: {
        system("cls");
        cout << "�" << setw(20) << "�.�.�." << setw(20) << "���������" << setw(30) << "��� ������������ � ������" << setw(15) << "�����������" << setw(12) << "����" << setw(13) << "�����" << endl;
        for (int i = 0; i < number.number; i++) {
            for (int j = i + 1; j < number.number; j++)
                if (base[i].experience > base[j].experience) {
                    swap(base[i].fio, base[j].fio);
                    swap(base[i].rank, base[j].rank);
                    swap(base[i].year, base[j].year);
                    swap(base[i].experience, base[j].experience);
                    swap(base[i].education, base[j].education);
                    swap(base[i].money, base[j].money);
                    j--;
                }
            cout << i + 1 << setw(20) << base[i].fio << setw(20) << base[i].rank << setw(20) << base[i].year << setw(23) << base[i].education << setw(12) << base[i].experience << setw(14) << base[i].money << endl;
        }
        system("pause");
        break;
    }
    case 5: {
        system("cls");
        cout << "�" << setw(20) << "�.�.�." << setw(20) << "���������" << setw(30) << "��� ������������ � ������" << setw(15) << "�����������" << setw(12) << "����" << setw(13) << "�����" << endl;
        for (int i = 0; i < number.number; i++) {
            for (int j = i + 1; j < number.number; j++)
                if (base[i].money > base[j].money) {
                    swap(base[i].fio, base[j].fio);
                    swap(base[i].rank, base[j].rank);
                    swap(base[i].year, base[j].year);
                    swap(base[i].experience, base[j].experience);
                    swap(base[i].education, base[j].education);
                    swap(base[i].money, base[j].money);
                    j--;
                }
            cout << i + 1 << setw(20) << base[i].fio << setw(20) << base[i].rank << setw(20) << base[i].year << setw(23) << base[i].education << setw(12) << base[i].experience << setw(14) << base[i].money << endl;
        }
        system("pause");
        break;
    }
    case 6: {
        system("cls");
        cout << "�" << setw(20) << "�.�.�." << setw(20) << "���������" << setw(30) << "��� ������������ � ������" << setw(15) << "�����������" << setw(12) << "����" << setw(13) << "�����" << endl;
        for (int i = 0; i < number.number; i++) {
            for (int j = i + 1; j < number.number; j++)
                if (strcmp(base[i].education.c_str(), base[j].education.c_str()) > 0) {
                    swap(base[i].fio, base[j].fio);
                    swap(base[i].rank, base[j].rank);
                    swap(base[i].year, base[j].year);
                    swap(base[i].experience, base[j].experience);
                    swap(base[i].education, base[j].education);
                    swap(base[i].money, base[j].money);
                    j--;
                }
            cout << i + 1 << setw(20) << base[i].fio << setw(20) << base[i].rank << setw(20) << base[i].year << setw(23) << base[i].education << setw(12) << base[i].experience << setw(14) << base[i].money << endl;
        }
        system("pause");
        break;
    }
    }
    return k = 1;
}

void menu() {
    int k = 1;
    while (k) {
        system("cls");
        int a = 0;
        cout << "�������� �������� : " << endl;
        cout << "1. ������� ����� �������� ���� � ������ ������." << endl;
        cout << "2. ������� ���������� � ����� �� �����." << endl;
        cout << "3. ����� �� �������� ����." << endl;
        cout << "4. �������� ����� �������� � ������������ ���� ����." << endl;
        cout << "5. ���������� �� �������� ����." << endl;
        cout << "6. �����. " << endl;
        cout << endl;
        while (a < 1 || a > 6 ) {
            cout << "��� ����� (1-6) : ";
            cin >> a;
        }
        switch (a)
        {
        case 1: {
            create(k);
            break;
        }
        case 2: {
            read(k);
            break;
        }
        case 3: {
            search(k);
            break;
        }
        case 4: {
            add(k);
            break;
        }
        case 5: {
            sorting(k);
            break;
        }
        case 6: {
            exit(1);
        }
        }
    }
}

int main() {
    setlocale(LC_ALL, "Russian");
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    menu();
    return 0;
}