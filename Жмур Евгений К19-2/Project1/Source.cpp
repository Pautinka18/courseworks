#include<iostream> 
#include <iomanip>  
#include <string> 
#include<windows.h> // ��� system("cls"); 
using namespace std;

struct strava
{
    string name;
    double price=0;
    double kalory;
};

struct obed {
    double summa=0;
    double sumkaloriy=0;
};

int main()
{
    SetConsoleOutputCP(1251);
    SetConsoleCP(1251);
    int N;
    int Q;
    cout << "������� ������ ����� : "; cin >> N;
    obed* temp = new obed[N];
    double min[1][2]{ 1000000,0 }, max[1][2] = { 0,0 }, maxcallor[1][3] = { 0,0,0 };
    cout << "������� ���� � ������ ����� : "; cin >> Q;
    strava **bludo = new strava*[N];
    for (int i = 0; i < N; i++) {
        bludo[i] = new strava[Q];
    }
    for (int i = 0; i < N; i++) {
        system("cls");
        for (int j = 0; j < Q; j++) {
            system("cls");
            cin.get();
            cout << "������� �������� " << j + 1 << " ����� " << i + 1 << " ����� : "; cin >> bludo[i][j].name;
            while (bludo[i][j].price < 1 || bludo[i][j].price > 1000) { cout << "������� ��������� " << j + 1 << " ����� " << i + 1 << " ����� : "; cin >> bludo[i][j].price; }
            cout << "������� ������������ " << j + 1 << " ����� " << i + 1 << " ����� : "; cin >> bludo[i][j].kalory;
            temp[i].summa += bludo[i][j].price;
            temp[i].sumkaloriy += bludo[i][j].kalory;
            if (maxcallor[0][0] < bludo[i][j].kalory) {
                maxcallor[0][0] = bludo[i][j].kalory;
                maxcallor[0][1] = i;
                maxcallor[0][2] = j;
            }
        }
        
        if (min[0][0] > temp[i].summa) {
            min[0][0] = temp[i].summa;
            min[0][1] = i;
        }
        if (max[0][0] < temp[i].summa) {
            max[0][0] = temp[i].summa;
            max[0][1] = i;
        }
    }
    system("cls");
    for (int i = 0; i < N; i++) {
        if (i == 0) {
            cout << "� �����" << setw(10) << "�����" << setw(10) << "����" << setw(17)<<"������������"<< endl;
            cout << "==============================================================================" << endl;
        }
        cout << setw(3) << i + 1 << setw(14) << bludo[i][0].name << setw(10) << bludo[i][0].price << setw(17) << bludo[i][0].kalory << endl;
        for (int j = 1; j < Q; j++) {
            cout << setw(17) << bludo[i][j].name << setw(10) << bludo[i][j].price << setw(17) << bludo[i][j].kalory << endl;
        }
    }
    cout << "==============================================================================" << endl;
    cout << "����� ������� ���� ��� ������� : " << max[0][1]+1 << ". ��� ��������� : " << max[0][0] <<"."<< endl;
    cout << "����� ������� ���� ��� ������� : " << min[0][1]+1 << ". ��� ��������� : " << min[0][0] << "." << endl;
    cout << "����� ���������� ����� ��������� � " << maxcallor[0][1] + 1 << " ����� �� " << maxcallor[0][2]+1 << " �������. ��� ������������� : " << maxcallor[0][0] << endl;
    for (int i = 0; i < N; i++) {
        delete[] bludo[i];
    }
    delete[] bludo;
    delete[] temp;
    system("pause");
    return 0;
}